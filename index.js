console.log("REST API")

// [SECTION] JavaScript Synchronous vs. Asynchronous
	// by default, JavaScript is synchronous meaning it only executes one statement at a time

console.log("Hello World!");

// conole.log("Hello World!");
/*for(let i=0;i <= 1500; i++){
	console.log(i);
}*/

console.log("I am after the error!");

// async and await - it lets us to proceed or execute more than one statements at the same time

// [SECTION] getting all posts
	// the fetch API allows us to assynchromously request for a resource or data
		// fetch() method in JavaScript is used to request to the server and oad information on the webpage
	/*
		Syntax: 
			fetch("apiURL");
	*/

	console.log(fetch("https://jsonplaceholder.typicode.com/posts"));

	/* a "promise" is an object that represesnts the eventual completion (or failure) of an asynchronous function and its resulting value */
	/*
		pending: initial states, neither fulfilled nor rejected response.
		fulfilled: operation was completed
		rejected: failed operation
	*/

	/*
		Syntax:
			fetch("apiURL")
			.then(response =>{what will do with the response})
	*/

	fetch("https://jsonplaceholder.typicode.com/posts")
	// the .then method captures the response object and returns another promise which will be either resolve or rejected
	// .then(response => response.json())
	.then(response => {
		console.log(response);
		return response.json()})
	.then(data => {
		let title = data.map(element => element.title)
		console.log(title);
	})

	//  the "async" and 'await' keyword to achieve asynchronous code

	async function fetchData(){
		let result = await(fetch("https://jsonplaceholder.typicode.com/posts"))
		console.log(result);
		let json = await result.json();
		console.log(json);
	}

	fetchData();

	// [SECTION] get a specific post
		// retrieves a specfici post following the REST API(/post/:id)
		// wildcard is where you can put any value, it then creates a link between id parameter in the url and the value provided in the URL

	fetch("https://jsonplaceholder.typicode.com/posts/3",{method:"GET"})
	.then(response => response.json())
	.then(result => console.log(result));

// [SECTION] Creating POST
	/*
		Syntax:
			fecth("apiURL",{necessaryOptions})
			.then(response => response.json())
			.then(result => {codeblock});
	*/

	fetch("https://jsonplaceholder.typicode.com/posts"
		, {
			// REST API methid to execute
			method: "POST",
			// headers, it tells us the data type of our request
			headers: {
				"Content-Type": "application/json"
			},
			// body - contains the data that will be added to the database/the request of the user
			body: JSON.stringify({
							title: "New Post",
							body: "Hello World",
							userId: 1
						})
		}
		)
	.then(response => response.json())
	.then(result => console.log(result));

// [SECTION] updating a post
	// put - full document
	fetch("https://jsonplaceholder.typicode.com/posts/5"
		, {
			method: "PUT",
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				title: "updated post",
				body: "Hello again",
				userId: 1
			})
		})
	.then(response => response.json())
	.then(result => console.log(result));

// [SECTION] Patch
	// patch - properties

	fetch("https://jsonplaceholder.typicode.com/posts/1",
		{
			method: "PATCH",
			headers: {
				'Content-Type': "application/json"
			},
			body: JSON.stringify({
				title: "Update Title!"
			})
		})
	.then(response => response.json())
	.then(result => console.log(result));

	// Mini-Activity
		// using the patch method, change the body property of the document which has the id of 18 to "Hello it's me!"


	fetch("https://jsonplaceholder.typicode.com/posts/18",
		{
			method: "PATCH",
			headers: {
				'Content-Type': "application/json"
			},
			body: JSON.stringify({
				body: "Hello it's me!"
			})
		})
	.then(response => response.json())
	.then(result => console.log(result));

// [SECTION] delete a post
	fetch("https://jsonplaceholder.typicode.com/posts/5",
			{
				method: "DELETE"
			}
		)
	.then(response => response.json())
	.then(result => console.log(result));